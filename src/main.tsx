import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'

import { CopilotKit } from "@copilotkit/react-core";
import { CopilotSidebar } from "@copilotkit/react-ui";
import "@copilotkit/react-ui/styles.css"; 

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <CopilotKit url="http://localhost:4201/api/copilotkit">
      <CopilotSidebar>
        <App />
      </CopilotSidebar>
    </CopilotKit>
  </React.StrictMode>,
)
